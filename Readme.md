Goatless
========

This is a tool for reading your livejournal friends posts in any RSS reader. 
Goatless generate static RSS-XML file, so you should have any "server" with public http to use it.

Installation
============

From rubygems:

    gem install goatless
    
From git:

    git clone git://github.com/semka/goatless.git goatless
    cd goatless
    gem build goatless.gemspec
    gem install goatless-x.x.gem

Usage
=====

    goatless -u lj-user-name -p lj-password -f file-to-write-rss-xml-feed
    
For more options see goatless --help.
Goatless is a command line application, so if you want to update your RSS-feed from time to time put something like this into your crontab:

    30 * * * * goatless -u sdfgh153 -p l00kMaUnbr3k@b1e! -f /var/www/friends.rss
