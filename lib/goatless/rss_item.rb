module Goatless

  class RssItem
    attr_accessor :pub_date, :xml_element, :security_type, :pub_date, :title, :body, :permalink, :author

    def public?
      @security_type == 'public'
    end

    def ==(an_rss_item)
      self.permalink == an_rss_item.permalink && !self.permalink.nil?
    end
  end 
end
