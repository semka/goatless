module Goatless
  class RssBuilder

    def initialize(items)
      @items = items
    end

    def document
      #TODO: Move number of items in feed into settings
      items = @items.sort { |x, y| x.pub_date <=> y.pub_date }.last(100).reverse
      
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |rss|
        rss.rss(:version => '2.0') {
          rss.channel {
            rss.title "Friends"
            rss.generator "Goatless"
            rss.description "Goatless friends"
            rss.link "https://github.com/semka/goatless"

            items.each do |i|
              rss.item {
                rss.pubDate i.pub_date.strftime("%a, %d %b %Y %H:%M:%S %z") # to RFC 822
                rss.author  i.author
                rss.link    i.permalink
                if i.public?
                  rss.title   i.title
                  rss.description do |d| d << i.body end
                else
                  rss.title  "Private post"
                  rss.description do |d| d << "Private post" end
                end
              }
            end
          }
        }
      end
      builder.to_xml
    end
  end 
end
