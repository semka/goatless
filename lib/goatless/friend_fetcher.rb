module Goatless

  class FriendFetcher
    attr_accessor :friend_url, :username, :password, :items

    def initialize(friend_url, username, password)
      if Goatless.options[:verbose]
        puts "Processing feed: #{friend_url}"
      end
      @friend_url = friend_url
      @username = username
      @password = password
    end

    def fetch
      @patron_session = Patron::Session.new
      @patron_session.username = @username
      @patron_session.password = @password
      @patron_session.timeout  = 10
      @patron_session.connect_timeout = 30000
      @patron_session.auth_type = :digest
      @items = []
      xml = @patron_session.get("#{@friend_url}?auth=digest").body
      parse(xml)
    end

    def parse(xml)
      xml_doc = Nokogiri::XML::Reader(xml)
      @storage = PStore.new('goatless.pstore') # Hope this can decrease memory usage

      max_items = 50 # Move this to the CLI flag or config
      current_item = nil
      current_author = nil

      xml_doc.each do |node|
        next if max_items == 0
        case node.name
        when 'lj:journal'
          current_author = node.inner_xml if current_author.nil?
        when 'item'
          if current_item.nil?
            current_item = RssItem.new
          else
            current_item.author = current_author
            if current_item.pub_date > (DateTime.now - 7) # do not add items older then week
              @storage.transaction do
                @storage[:items] ||= Array.new
                unless @storage[:items].include? current_item
                  @storage[:items] << current_item
                  @storage.commit
                end
              end
            end
            current_item = nil
            max_items -= 1
          end
        when 'link'
          unless current_item.nil?
            current_item.permalink = node.inner_xml if current_item.permalink.nil?
          end
        when 'pubDate'
          current_item.pub_date = DateTime.parse(node.inner_xml) if current_item.pub_date.nil?
        when 'description'
          unless current_item.nil?
            if current_item.body.nil?
              current_item.body = node.inner_xml
            end
          end
        when 'lj:security'
          current_item.security_type = node.inner_xml if current_item.security_type.nil?
        when 'title'
          unless current_item.nil?
            current_item.title = node.inner_xml if current_item.title.nil?
          end
        end
      end
    end
  end
end
