module Goatless

  class Goatless

    def initialize(options)
      @@options = options
      @username, @password = options[:username], options[:password]
      @threads = []
      friends_urls = OpmlFetcher.new(@username).fetch
      friends_urls.each_slice(3) do |rss_urls|
        @threads << Thread.new {
          rss_urls.each do |rss_url|
            friend_fetcher = FriendFetcher.new(rss_url, @username, @password)
            begin
              friend_fetcher.fetch
            rescue Exception => e
              # Still have a lot's of "connection timed out" here
              $stderr.puts("#{rss_url} fetching failed with error: #{e}")
            end
          end
        }
      end
      @threads.each { |t| t.join }
    end

    def self.options
      @@options
    end

    def rss
      @storage = PStore.new('goatless.pstore')
      @storage.transaction(true) do
        RssBuilder.new(@storage[:items]).document
      end
    end
  end
end
