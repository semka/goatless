module Goatless
  class OpmlFetcher
    attr_accessor :rss_urls

    def initialize(username)
      @url = "http://www.livejournal.com/tools/opml.bml?user=#{username}"
      # @url = 'http://sdfgh153.ru/sample.opml'
    end

    def fetch
      patron_session = Patron::Session.new
      patron_session.timeout = 30
      patron_session.connect_timeout = 30000
      @rss_urls = []

      opml_doc = Nokogiri::XML(patron_session.get(@url).body)
      opml_doc.elements.xpath("//outline").each do |outline|
        @rss_urls << outline['xmlUrl']
      end
      if Goatless.options[:verbose]
        puts "You have #{@rss_urls.size} friends!"
      end
      @rss_urls
    end
  end
end
