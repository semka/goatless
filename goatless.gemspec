# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = %q{goatless}
  s.version = "0.1.21"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = [%q{Semka Novikov}]
  s.date = %q{2011-08-19}
  s.email = %q{semka.novikov@gmail.com}
  s.executables = [%q{goatless}]

  s.files = [%q{bin/goatless}, 
             %q{lib/goatless},
             %q{lib/goatless/friend_fetcher.rb}, 
             %q{lib/goatless/goatless.rb}, 
             %q{lib/goatless/opml_fetcher.rb}, 
             %q{lib/goatless/rss_builder.rb}, 
             %q{lib/goatless/rss_item.rb},
             %q{lib/goatless.rb}]

  s.homepage = %q{http://github.com/semka/goatless/}
  s.require_paths = [%q{lib}]
  s.rubygems_version = %q{1.8.5}

  s.summary = "Liberate livejournal friends from the Frank's yoke"
  s.description = "Tool for translation Livejournal.com friends into RSS-feed"

  s.add_dependency('patron',   '>= 0.4.13')
  s.add_dependency('nokogiri', '>= 1.5.0')

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
